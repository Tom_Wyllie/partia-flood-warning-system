"""This module contains a collection of functions related to
geographical data.

"""
import math


def stations_by_distance(stations, p):
    station_distances = ((station, haversine_formula(station.coord, p))
                         for station in stations)
    return sorted(station_distances, key=lambda x: x[1])


def haversine_formula(p1, p2):
    lat1, long1, lat2, long2 = map(math.radians, p1 + p2)
    d_lat = lat2 - lat1
    d_long = long2 - long1
    earth_radius_km = 6371.
    hav_dr = ((math.sin(d_lat / 2.) ** 2) + math.cos(lat1) * math.cos(lat2) *
              (math.sin(d_long / 2.) ** 2))
    return 2. * earth_radius_km * math.asin(math.sqrt(hav_dr))


def stations_within_radius(stations, centre, r):
    stations_within_range = []

    station_distances = ((station, haversine_formula(station.coord, centre))
                         for station in stations)

    stations_within_range = [station[0]
                             for station in station_distances if station[1] < r]
    return stations_within_range


def rivers_with_station(stations):
    # Creates a set with the names of rivers which are covered by stations
    rivers = {station.river for station in stations}
    return rivers


def stations_by_river(stations):
    river_to_station_dict = {}
    for station in stations:
        if station.river in river_to_station_dict:
            river_to_station_dict[station.river].append(station)

        else:
            river_to_station_dict[station.river] = [station]

    return river_to_station_dict


def rivers_by_station_number(stations, N):
    river_to_station = stations_by_river(stations)

    # Making the list with all rivers with number of stations paired as tuples
    rivers_to_num_stations = sorted([(river, len(stations))
                                     for river, stations in
                                     river_to_station.items()],
                                    key=lambda x: -x[1])

    # Finding the lowest number of stations for which river should be included:
    least_station_cutoff = rivers_to_num_stations[N - 1][1]

    most_covered_n_rivers = [river_tuple
                             for river_tuple in rivers_to_num_stations
                             if river_tuple[1] >= least_station_cutoff]

    return most_covered_n_rivers
