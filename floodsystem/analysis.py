import numpy as np


# noinspection PyUnusedLocal
def polyfit(dates, levels, p):
    # Data should be linear TODO could be improved

    # Create set of 10 data points on interval (1000, 1002)
    x = np.arange(start=0, stop=len(levels), step=1)
    y = levels

    # Using shifted x values, find coefficient of best-fit
    # polynomial f(x) of degree 4
    p_coeff = np.polyfit(x - x[0], y, 4)

    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    return np.poly1d(p_coeff)
