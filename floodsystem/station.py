"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    # TODO define using keyword arguments
    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):
        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        # The attributes of the station to print out, and the label to go along
        #   side it. Formatting a single string is far more efficient than
        #   line-by-line concatenation, which has O(n^2) complexity.
        return ('\nStation name:                {s.name}\n'
                '    ID:                      {s.station_id}\n'
                '    Measure ID:              {s.measure_id}\n'
                '    Coordinate:              {s.coord}\n'
                '    Town:                    {s.town}\n'
                '    River:                   {s.river}\n'
                '    Typical Range:           {s.typical_range}').format(s=self)

    @property
    def json_data(self):
        return {'station_id': self.station_id,
                'name': self.name,
                'coord': self.coord,
                'typical_range': self.typical_range,
                'latest_level': self.latest_level,
                'relative_level': self.relative_water_level()}

    @property
    def lat_lng_dict(self):
        return {'lat': self.coord[0],
                'lng': self.coord[1]}

    def relative_water_level(self, level_value=None):
        if level_value is None:
            level_value = self.latest_level
        if level_value is not None and self.typical_range_consistent():
            typical_domain = self.typical_range[1] - self.typical_range[0]
            return (level_value - self.typical_range[0]) / typical_domain

    def typical_range_consistent(self):
        # Try consistency check to see if data is inconsistent
        try:
            # We must only proceed to the consistency check if the data exists
            return self.typical_range[0] < self.typical_range[1]
        except (IndexError, KeyError, TypeError):
            pass
        return False

    def relative_water_level(self):
        # Check if relevant data is available
        if self.latest_level is None or not self.typical_range_consistent():
            return None

        return (self.latest_level - self.typical_range[0]) / (self.typical_range[1] - self.typical_range[0])


def inconsistent_typical_range_stations(stations):
    return [stn for stn in stations if not stn.typical_range_consistent()]
