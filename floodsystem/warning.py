from floodsystem.datafetcher import fetch_measure_levels
import datetime


def station_to_warning(station):
    # Get a list of water levels from the past ten days

    dates, water_levels = fetch_measure_levels(station.measure_id,
                                               dt=datetime.timedelta(days=10))

    if not station.typical_range_consistent() or len(water_levels) == 0:
        return "data for station inconsistent/non_existent"

    high_water_level_counter = 0

    for water_level in water_levels:
        if type(water_level) is list:
            water_level = water_level[0]
        current_relative_level = (water_level - station.typical_range[0]) / (
                station.typical_range[1] - station.typical_range[0])

        if current_relative_level > station.typical_range[1]:
            high_water_level_counter += 1

    proportion_high_water_levels = high_water_level_counter / len(water_levels)

    warning = "no level available"
    if proportion_high_water_levels > 0.9:
        warning = "severe"

    if 0.7 < proportion_high_water_levels <= 0.9:
        warning = "high"

    if 0.5 < proportion_high_water_levels <= 0.7:
        warning = "moderate"

    if proportion_high_water_levels <= 0.5:
        warning = "low"

    return warning
