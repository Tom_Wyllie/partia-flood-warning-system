from floodsystem.stationdata import build_station_list, update_water_levels
import hdbscan


def cluster(stations):
    cleaned_stations = [station for station in stations if station.relative_water_level() is not None]

    x_values = [station.coord[0] for station in cleaned_stations]
    y_values = [station.coord[1] for station in cleaned_stations]
    relative_water_levels = [station.relative_water_level() for station in cleaned_stations]

    x_min = min(x_values)
    y_min = min(y_values)

    x_range = max(x_values) - min(x_values)
    y_range = max(y_values) - min(y_values)

    max_coordinate_range = max([x_range, y_range])

    max_relative_water_level = max(relative_water_levels)
    min_relative_water_level = min(relative_water_levels)

    station_features = []

    for station in cleaned_stations:
        x_normalised = (station.coord[0] - x_min) / max_coordinate_range
        y_normalised = (station.coord[1] - y_min) / max_coordinate_range
        relative_water_level_normalised = 3 * (station.relative_water_level() - min_relative_water_level) / (
                max_relative_water_level - min_relative_water_level)

        data_point = [x_normalised, y_normalised, relative_water_level_normalised]

        station_features.append(data_point)

    clusterer = hdbscan.HDBSCAN(min_cluster_size=3, gen_min_span_tree=True)
    clusterer.fit(station_features)

    clustered_data = zip(cleaned_stations, clusterer.labels_)

    return list(clustered_data)
