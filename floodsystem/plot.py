import plotly.plotly as py
import plotly.graph_objs as go

import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels


def plot_water_levels(station, dates, levels):
    trace_low = go.Scatter(
        x=dates,
        y=[station.typical_range[0]]*len(dates),
        name="Typical Low Water Level",
        line = dict(
            shape = 'linear',
            color ='rgb(0, 0, 200)',
            dash='dash'
        )
    )

    trace_high = go.Scatter(
        x=dates,
        y=[station.typical_range[1]]*len(dates),
        name="Typical High Water Level",
        line = dict(
            shape = 'linear',
            color ='rgb(200, 0, 0)',
            dash = 'dash'
        )
    )

    trace_actual_level = go.Scatter(
        x=dates,
        y=levels,
        name="Current Level",
        line = dict(
            shape = 'linear',
            color ='rgb(0, 200, 0)'
        )
    )

    data = [trace_actual_level, trace_low, trace_high]

    layout = go.Layout(
        title=station.name,

        xaxis=dict(
            title='Date',
            titlefont=dict(
                family='Courier New, monospace',
                size=18,
                color='#7f7f7f'
            )
        ),
        yaxis=dict(
            title='Water level (m)',
            titlefont=dict(
                family='Courier New, monospace',
                size=18,
                color='#7f7f7f'
            )
        )
    )

    fig = go.Figure(data=data, layout=layout)

    py.plot(fig, filename=station.name, sharing="public")
