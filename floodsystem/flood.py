"""This module contains a collection of functions related to
flood levels.

"""


def stations_level_over_threshold(stations, tol):
    stations_over_tolerance = [station for station in stations if
                               station.relative_water_level() is not None
                               and station.relative_water_level() > tol]

    stations_over_tolerance.sort(key=lambda
        x: x.relative_water_level(), reverse=True)

    return [(station, station.relative_water_level())
            for station in stations_over_tolerance
            ]


def stations_highest_rel_level(stations, N):
    stations = [station for station in stations if
                station.relative_water_level() is not None]

    stations.sort(key=lambda x: x.relative_water_level(), reverse=True)
    return stations[0: N]
