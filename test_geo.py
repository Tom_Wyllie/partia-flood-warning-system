from floodsystem import geo
import collections
from random import shuffle
from floodsystem.station import MonitoringStation


def dummy_station_list_creator():
    test_station1 = MonitoringStation("001", "m-id-1", "station-1",
                                      (1, 1), (1, 1), "Mississippi", "Jackson")

    test_station2 = MonitoringStation("002", "m-id-2", "station-2",
                                      (1, 2), (0.5, 1.5), "Nile", "Cairo")

    test_station3 = MonitoringStation("003", "m-id-3", "station-3",
                                      (2, 34), (234, 4), "Nile", "Khartoum")

    test_station4 = MonitoringStation("004", "m-id-4", "station-4",
                                      (2, 34), (234, 4), "Thames", "London")

    test_station5 = MonitoringStation("005", "m-id-5", "station-5",
                                      (2, 34), (234, 4), "Thames", "Reading")

    return [test_station1, test_station2, test_station3, test_station4,
            test_station5]


def test_haversine_formula():
    # These coordinates and this answer are known to be correct
    p1 = (36.12, -86.67)
    p2 = (33.94, -118.40)
    assert round(geo.haversine_formula(p1, p2), 4) == 2886.4444
    assert round(geo.haversine_formula(p2, p1), 4) == 2886.4444


def test_stations_by_distance():
    centre_coords = (1, 1)
    s_close = DummyStation((3, 4))
    s_mid = DummyStation((7, 3))
    s_far = DummyStation((11, 19))

    misordered_stations = (s_close, s_far, s_mid)
    stations = geo.stations_by_distance(misordered_stations, centre_coords)

    # Sanity check distances
    assert stations[0][1] < stations[1][1] < stations[2][1]

    # Verify stations have been ordered correctly
    assert stations[0][0] is s_close
    assert stations[1][0] is s_mid
    assert stations[2][0] is s_far


# noinspection PyArgumentList
def test_stations_within_radius():
    # representative radius of 500km and central coordinate of (0,0)
    centre_coords = (0, 0)
    r = 500

    # creating lists of stations known to be in and out of range
    s_in_range = [DummyStation((1, 1)), DummyStation((0.5, 2)),
                  DummyStation((0.1, 4))]

    s_out_of_range = [DummyStation((10, 10)), DummyStation((43, 27)),
                      DummyStation((5, 2))]

    representative_s = s_out_of_range + s_in_range
    shuffle(representative_s)

    c_s_in_range = geo.stations_within_radius(representative_s,
                                              centre_coords,
                                              r)

    assert collections.Counter(c_s_in_range) == collections.Counter(s_in_range)


# noinspection PyArgumentList
def test_rivers_with_station():
    test_stations = dummy_station_list_creator()

    rivers_with_station = geo.rivers_with_station(test_stations)

    assert type(type(rivers_with_station) is set)

    test_values = collections.Counter(rivers_with_station)
    known_values = collections.Counter({"Mississippi", "Nile", "Thames"})
    assert test_values == known_values


def test_stations_by_river():
    test_stations = dummy_station_list_creator()
    river_to_s_dict = geo.stations_by_river(test_stations)

    assert river_to_s_dict["Thames"] == [station for station
                                         in test_stations
                                         if station.river
                                         == "Thames"]

    assert river_to_s_dict["Nile"] == [station for station
                                       in test_stations
                                       if station.river
                                       == "Nile"]

    assert river_to_s_dict["Mississippi"] == [station for station
                                              in test_stations
                                              if station.river
                                              == "Mississippi"]


# noinspection PyArgumentList
def test_rivers_by_station_number():
    # Get most 'populated' river, which will be both Thames and Nile:
    test_stations = dummy_station_list_creator()

    most_populated_river = geo.rivers_by_station_number(test_stations, 1)

    test_values = collections.Counter([("Thames", 2), ("Nile", 2)])
    known_values = collections.Counter(most_populated_river)
    assert test_values == known_values


class DummyStation:
    """
    A dummy station class used to test the geo module
    """

    def __init__(self, p):
        assert type(p) is tuple
        self.coord = p
