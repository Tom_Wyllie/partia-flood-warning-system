from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.clustering import cluster
import plotly.plotly as py
from plotly.graph_objs import *
import collections
import random


def run():
    stations = build_station_list()
    update_water_levels(stations)

    # obtain a list of tuples, one element being the station and the other the cluster ID
    stations_with_cluster_ID = cluster(stations)

    clustered_stations = collections.defaultdict(list)

    # Separated stations to give a diction with id as key and a list of stations in that cluster
    for station, cluster_ID in stations_with_cluster_ID:
        clustered_stations[cluster_ID].append(station)

    # Initialise the plotting data array:
    data_list = []

    for cluster_ID, station_list in clustered_stations.items():
        if cluster_ID == -1:
            color = 'rgb(211,211,211)'
            cluster_name = 'Noise'

        else:
            random_color = random.randint(1, 361)
            color = 'hsv(%s, 0.5, 0.95)'.format(random_color)
            cluster_name = "Cluster {}".format(cluster_ID)

        trace = Scattermapbox(
            lat=[station.coord[0] for station in station_list],
            lon=[station.coord[1] for station in station_list],
            mode='markers',
            marker=Marker(
                size=9,
                color=color
            ),
            text=[station.name for station in station_list],
            name=cluster_name
        )

        data_list.append(trace)

    mapbox_access_token = 'pk.eyJ1IjoiaHNhZGRpcSIsImEiOiJjamR5eWRydG0xOHI4MnlxcGF3cHViNDQ4In0.HXWEr-B7DM7Hxs1vk2aeiw'

    data = Data(data_list)

    layout = Layout(
        autosize=True,
        hovermode='closest',
        mapbox=dict(
            accesstoken=mapbox_access_token,
            bearing=0,
            center=dict(
                lat=52.561928,
                lon=-1.464854
            ),
            pitch=0,
            zoom=10
        ),
    )

    fig = dict(data=data, layout=layout)
    py.plot(fig, filename='Stations')


if __name__ == "__main__":
    print("*** Clustering Demo: CUED Part IA Flood Warning System ***")
    run()
