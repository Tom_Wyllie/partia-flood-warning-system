from floodsystem import geo
from floodsystem import stationdata


def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = stationdata.build_station_list()

    # Build sorted list of (MonitoringStation object, distance) tuples
    camb = (52.2053, 0.1218)
    distance_sorted_stations = geo.stations_by_distance(stations, camb)

    # Closest stations
    closest_stations = extract_station_data(distance_sorted_stations[:10])
    write_output(closest_stations, '\n\nClosest stations (km) to', camb)

    furthest_stations = extract_station_data(distance_sorted_stations[-10:])
    write_output(furthest_stations, '\n\nFurthest stations (km) from', camb)


def extract_station_data(stations):
    return [(station.name, station.river, distance)
            for station, distance in stations]


def write_output(stations, msg, coords):
    # The header is a single item list so it can be concatenated to the body
    header_msg = [msg + ' {:.5f}, {:.5f};'.format(*coords)]
    body_msgs = ['{:32} {:32}{:6.3f}'.format(*station) for station in stations]
    print('\n  '.join(header_msg + body_msgs))


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()
