function startDecomp() {
    fetch('/polygons').then(function(response) {
        return response.text();
    }).then(function(b64Response) {
        var polygonData = decodeB64ToJSON(b64Response);

        // Load the colourmap, badly
        fetch('/colourmap').then(function(response) {
            return response.json();
        }).then(function(colourMap) {

            // Load in the timestamps for the animation
            var timestamps = {};
            fetch('/timestamps').then(function(response) {
                return response.json();
            }).then(function (timestamps) {
                console.log(timestamps);
                initMap(polygonData, colourMap, timestamps);
            });
        });
    });
}

function decodeB64ToJSON(b64) {
    s = atob(b64);

    var data = new Array(s.length);
    for (var i = 0; i < s.length; i += 1) {
        data[i] = s.charCodeAt(i);
    }

    var inflated = pako.inflate(data);
    var plainText = new TextDecoder("utf-8").decode(inflated);
    return JSON.parse(plainText);
}

function initMap(polygonData, colourMap, timestamps) {
    // Create a new StyledMapType object, passing it an array of styles,
    // and the name to be displayed on the map type control.
    var styledMapType = new google.maps.StyledMapType(
        // Styles created using https://mapstyle.withgoogle.com/
        [
    {
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#f5f5f5"
    }
    ]
    },
    {
    "elementType": "labels.icon",
    "stylers": [
    {
    "visibility": "off"
    }
    ]
    },
    {
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#616161"
    }
    ]
    },
    {
    "elementType": "labels.text.stroke",
    "stylers": [
    {
    "color": "#f5f5f5"
    }
    ]
    },
    {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#bdbdbd"
    }
    ]
    },
    {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#eeeeee"
    }
    ]
    },
    {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#757575"
    }
    ]
    },
    {
    "featureType": "poi.business",
    "stylers": [
    {
    "visibility": "off"
    }
    ]
    },
    {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#e5e5e5"
    }
    ]
    },
    {
    "featureType": "poi.park",
    "elementType": "labels.text",
    "stylers": [
    {
    "visibility": "off"
    }
    ]
    },
    {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#9e9e9e"
    }
    ]
    },
    {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#ffffff"
    }
    ]
    },
    {
    "featureType": "road.arterial",
    "elementType": "labels",
    "stylers": [
    {
    "visibility": "off"
    }
    ]
    },
    {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#757575"
    }
    ]
    },
    {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#dadada"
    }
    ]
    },
    {
    "featureType": "road.highway",
    "elementType": "labels",
    "stylers": [
    {
    "visibility": "off"
    }
    ]
    },
    {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#616161"
    }
    ]
    },
    {
    "featureType": "road.local",
    "stylers": [
    {
    "visibility": "off"
    }
    ]
    },
    {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#9e9e9e"
    }
    ]
    },
    {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#e5e5e5"
    }
    ]
    },
    {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#eeeeee"
    }
    ]
    },
    {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
    {
    "color": "#c9c9c9"
    }
    ]
    },
    {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
    {
    "color": "#9e9e9e"
    }
    ]
    }
    ],
        {name: 'Map'});

    // Create a map object, and include the MapTypeId to add
    // to the map type control.
    var map = new google.maps.Map(document.getElementById('map_div'), {
      // Somewhere nice and central
      center: {lat: 52.758, lng: -2.0},
      zoom: 7,
      mapTypeControlOptions: {
        mapTypeIds: ['styled_map']
      }
    });
    var polygons = initialisePolygons(polygonData, map);

    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

    var animationSlider = document.getElementById('animation_slider');
    var timestampSpan = document.getElementById('timestamp');

    var durationMs = 12000
    var frame, fps, fpsInterval, startTime, now, then, elapsed, allowNextFrame, progress;
    var numReadings = polygonData[0]['readings'].length;

    function startAnimating(fps) {
        fpsInterval = 1000 / fps;
        then = Date.now();
        startTime = then;
        allowNextFrame = true;
        animate(fpsInterval);
    }

    function animate() {
        // request another frame
        setTimeout(function() {
            if(allowNextFrame) {
                requestAnimationFrame(animate);
            }
        }, fpsInterval);

        // calc elapsed time since last loop
        now = Date.now();
        elapsed = now - then;

        // if enough time has elapsed, draw the next frame
        if (elapsed > fpsInterval) {
            // Get ready for next frame by setting then=now, but also adjust for the
            // specified fpsInterval not being a multiple of RAF's interval (16.7ms)
            then = now - (elapsed % fpsInterval);

            // Render map
            progress = (now - startTime) / durationMs;
            progress = Math.min(1.0, progress);
            frame = parseInt(numReadings * progress);
            sliderValue = parseInt(999.0 * progress)
            animationSlider.value = sliderValue;

            if(frame < numReadings) {
                timestampSpan.innerHTML = timestamps[frame];
                renderPolygons(polygons, map, polygonData, colourMap, frame)
                console.log('Animation running...')
            } else {
                allowNextFrame = false;
                console.log('Animation complete')
            }
        }
    }

    // Bind listeners
    document.getElementById('animate_button').addEventListener('click', function() {
        startAnimating(30);
    });

    animationSlider.oninput = function() {
        if(allowNextFrame) {
            allowNextFrame = false;
        }
        var frame = parseInt(numReadings * (this.value / 1000.0));
        frame = Math.min(frame, numReadings-1);
        console.log(timestamps[frame]);
        console.log(timestampSpan);
        timestampSpan.innerHTML = timestamps[frame];
        console.log(timestampSpan);
        renderPolygons(polygons, map, polygonData, colourMap, frame);
    }

    renderPolygons(polygons, map, polygonData, colourMap, numReadings-1);
    timestampSpan.innerHTML = timestamps[numReadings-1];
}

function renderPolygons(polygons, map, polygonData, colourMap, frame) {
    for(var i = 0; i < polygons.length; i++) {
        // Update colour of polygon
        polygons[i].setOptions(colourMap[polygonData[i]['readings'][frame]]);
    }
}

function initialisePolygons(polygonData, map) {
    polygons = [];
    for(var i = 0; i < polygonData.length; i++) {
        var riverPolygon = new google.maps.Polygon({
            paths: polygonData[i]['coord_path'],
            strokeWeight: 0.0,
            fillColor: '#ffff00',
            fillOpacity: 0.0
        });
        riverPolygon.setMap(map);
        polygons.push(riverPolygon);
    }
    return polygons
}
