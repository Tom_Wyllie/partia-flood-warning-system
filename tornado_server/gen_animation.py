# noinspection PyUnresolvedReferences,PyPackageRequirements
from merge import polygon_merger

# noinspection PyUnresolvedReferences
from floodsystem_linked import stationdata

import numpy as np

# noinspection PyPackageRequirements
import matplotlib.pyplot as plt


def main():

    stations = stationdata.build_station_list()
    coords = [stn.coord for stn in stations]
    coords = np.asarray(coords)

    print('Merging polygons...')
    all_polygons = polygon_merger.merge_polygons(coords,
                                                 requested_polygons=1,
                                                 max_d=0.6,
                                                 keep_all_iters=True)

    num = len(all_polygons)
    out = []
    out += list(range(20))
    out += [2 * i + 20 for i in range(25)]
    for index in range(10, num):
        frame = int(index ** 1.9)
        if frame < num:
            out.append(frame)
        else:
            break
    out.append(num-1)

    for count, i in enumerate(out[::-1]):
        print('Printing frame {:d}, iteration {:d}'.format(count, num-i))
        save_polygons(coords, all_polygons[::-1][i], str(count),
                      iter_number=num-i)


def save_polygons(coords, polygons, file_name, iter_number):
    print('Plotting polygons...')
    fig = plt.figure(figsize=(10, 10))
    for p in polygons.values():
        path = [coords[v][::-1] for v in p.path]
        path.append(coords[p.path[0]][::-1])
        x = [coord[0] for coord in path]
        y = [coord[1] for coord in path]
        plt.plot(x, y, 'coral')
    plt.title('Polygon merge iteration {:d}'.format(iter_number))
    plt.savefig('anim_out/out_{}.png'.format(file_name))
    plt.close(fig)


if __name__ == '__main__':
    main()
