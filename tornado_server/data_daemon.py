"""This script should run daily to process the data downloaded from the online
    flood API, and generate the files the server requires to host the website"""
import argparse
import datetime
import json
import os

import numpy as np

# noinspection PyUnresolvedReferences
from floodsystem_linked import stationdata


INTERVAL_MINUTES = 15
INTERVAL_SECONDS = 60 * INTERVAL_MINUTES
# If a station has missed 10 or more readings we're not interested; the data
#   is too patchy and will cause flickering on the heatmap
MAX_MISSED_READINGS = 10
MAX_POLYGON_SIZE = 0.6


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--days', default=7)
    parser.add_argument('--path', default='downloaded_data')
    parser.add_argument('--polys', default=360)
    args = parser.parse_args()

    download_data(int(args.polys), previous_days=args.days, data_path=args.path)


def download_data(num_poly, previous_days, data_path):
    # TODO this isn't foolproof
    data_path_template = data_path + '/{}.json'

    # === INITIALISE VARIOUS DATA STRUCTURES ===
    today = datetime.date.today()
    previous_dates = [today - datetime.timedelta(i + 1)
                      for i in range(previous_days)]

    # Calculate how many different timestamps can be present in the data
    midnight = datetime.datetime.min.time()
    earliest_timestamp = datetime.datetime.combine(previous_dates[-1], midnight)
    num_timestamps = (previous_days * 24 * 60) // INTERVAL_MINUTES

    # Generate list of stations using the floodsystem module
    stations = stationdata.build_station_list()

    # Map each measure ID to its corresponding station
    id_station_map = {cleanup_id(s.measure_id): s for s in stations}

    # Map each measure ID to a column index value for the numpy matrix
    index_map = {stn_id: i for i, stn_id in enumerate(id_station_map.keys())}

    # Remap each integer index *back* to the ID key that lead to it; this makes
    #   our dictionary bidirectional - BEWARE - this is risky if there can be
    #   any overlap between keys and values; however all IDs are strings and all
    #   indices are integers, so this shouldn't cause issues, but let's do a
    #   test ~just in case~ it does.
    id_map = {index_map[stn_id]: stn_id for stn_id in index_map}
    assert not set(id_map.keys()) & set(index_map.keys())
    id_index_bi_map = {**index_map, **id_map}

    # === OPEN THE LARGE AMOUNT OF DATA DOWNLOADED FROM THE ENVIRONMENT API ===
    responses = []
    for raw_date in previous_dates:
        iso_date = raw_date.isoformat()
        print('Reading cached data for {date}...'.format(date=iso_date))

        with open(data_path_template.format(iso_date)) as f_in:
            responses.append(json.loads(f_in.read()))

    assert responses

    # === PROCESS THE DATA ===
    # Immediately clean up the data to reduce memory usage, as we don't need
    #   the vast majority of the data;
    time_series_matrix = np.empty(shape=(num_timestamps, len(stations)))
    time_series_matrix[:] = np.NaN
    timestamp_format = '%Y-%m-%dT%H:%M:%SZ'

    readings_without_metadata_ids = set()

    # It is known that the first 62 characters of the ID are always the same,
    #   and the ID always terminates with a '-' describing the next field
    for response in responses:
        response_items = response['items']
        for item in response_items:
            item_id = cleanup_id(item['@id'], n=62)
            try:
                column_index = id_index_bi_map[item_id]
            # Sometimes there exists reading for stations we have no data on,
            #   these just have to be ignored
            except KeyError:
                readings_without_metadata_ids.add(item_id)
                continue

            try:
                # Parse the current level into a relative level integer
                station = id_station_map[item_id]
                data_reading = item['value']
            except KeyError:
                print('WARNING: bad data for station ID {}'.format(item_id))
                continue

            # Account for inconsistency in environment API data; the 'value'
            #   field is sometimes given as a list (inexplicably...)
            if type(data_reading) is list:
                data_reading = data_reading[1]
            rel_level = station.relative_water_level(data_reading)

            # station.relative_water_level will return None in the case of an
            #   inconsistent typical range; readings cannot be used for these
            #   stations.
            if rel_level is None:
                continue

            # Scale value of reading from 1 - 255. 255 Implies the water level
            #   was at or above the maximum threshold, level of 0 implies level
            #   was at or below the minimum threshold, level of NaN is reserved
            #   for missing readings.
            level_integer = 0
            if rel_level >= 1:
                level_integer = 255
            elif rel_level > 0:
                level_integer = int(rel_level * 255)

            # Convert timestamp into datetime object
            ts = datetime.datetime.strptime(item['dateTime'], timestamp_format)

            # Find the appropriate row of the matrix for this data entry
            dt = ts - earliest_timestamp
            row_index = int(dt.total_seconds() // INTERVAL_SECONDS)
            time_series_matrix[row_index][column_index] = level_integer

    # Alert user to the station IDs that weren't found in the list of
    #   MonitoringStation objects, but that readings were detected for.
    if readings_without_metadata_ids:
        print('WARNING: no data found for stations;\n\t{}'.format(
            '\n\t'.join(readings_without_metadata_ids)))

    # === FROM THE PROCESSED DATA, GENERATE THE POLYGON DATA FOR GMAPS ===
    # Create an ndarray of the lat / long coords for each station
    # coords = [id_station_map[id_index_bi_map[i]].coord
    #           for i in range(len(stations))]

    # Ensure that the index of each station corresponds to its integer ID in the
    #   bidirectional map.
    coords = [id_station_map[id_index_bi_map[i]].coord
              for i in range(len(stations) - 1)]
    coords = np.asarray(coords)

    # noinspection PyUnresolvedReferences,PyPackageRequirements
    from merge import polygon_merger

    print('Merging polygons...')
    polygons = polygon_merger.merge_polygons(coords,
                                             requested_polygons=num_poly,
                                             max_d=0.6)

    # For each polygon, construct a list of data readings at each possible
    #   timestamp. The data reading for a polygon at each timestamp is equal to
    #   the mean of the normalised readings for each station that makes up the
    #   members of the polygon, excluding any NaN values as these denote missing
    #   readings. If all values are Nan then the final polygon reading is
    #   given by a None - which denotes that the polygon should be completely
    #   transparent when rendered by the client. This is translated to a null
    #   when dumped into JSON.
    output_polygons = []
    for polygon in polygons:
        # Make a list of lat_lng objects that the javascript client can use to
        #   render the map. Use generator expressions for efficiency; we only
        #   actually loop through the stations once here.
        vertex_ids = (id_index_bi_map[v] for v in polygons[polygon].path)
        vertex_stations = (id_station_map[i] for i in vertex_ids)
        coord_path = [stn.lat_lng_dict for stn in vertex_stations]

        # The polygon's path must close
        coord_path.append(coord_path[0])

        # The values stored in the members set of each polygon corresponds to
        #   the column index of the readings of the station at that vertex, in
        #   the big data matrix.
        members_vertices = list(polygons[polygon].members)
        vertex_readings = time_series_matrix[:, members_vertices]
        readings = list(np.nanmean(vertex_readings, axis=1))

        # Remove any NaN values; this is a bit whacky - this property ('x != x')
        #   is part of the IEEE 754 specification on floats.
        readings = [int(x) if x == x else None for x in readings]

        missed_readings = sum(1 for reading in readings if reading is None)
        if missed_readings > MAX_MISSED_READINGS:
            readings = [None for _ in readings]

        polygon_data = {'coord_path': coord_path,
                        'readings': readings}
        output_polygons.append(polygon_data)

    # === FINALLY WRITE OUT THE CLEANED AND PROCESSED DATA ===
    with open('polygons.json', 'w') as f:
        f.write(json.dumps(output_polygons))


def cleanup_id(raw_id, n=60):
    # Parse and format station id
    return raw_id[n:].split('-')[0]


if __name__ == '__main__':
    main()
