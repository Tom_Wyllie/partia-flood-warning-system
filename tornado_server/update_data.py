"""This script should run daily to downloaded the most recent data from
    the online flood API, and ensure there is data for all previous days
    requested. Also purge old downloaded files, so that the downloads directory
    contains only the most up to date data from the environment API."""
import argparse
import datetime
import json
import os
import requests

GET_URL_TEMPLATE = ('https://environment.data.gov.uk/flood-monitoring/data'
                    '/readings?date={date}&parameter=level&qualifier=Stage')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--days', default=7)
    parser.add_argument('--path', default='downloaded_data')
    args = parser.parse_args()

    if not os.path.isdir(args.path):
        raise RuntimeError('Specified folder was not yet created')

    today = datetime.date.today()
    previous_dates = [today - datetime.timedelta(i+1) for i in range(args.days)]

    # Make sure there are no files other than the dates to be downloaded present
    #    in the downloads folder.
    legal_paths = set()

    for iso_date in previous_dates:
        data_file_path = os.path.join(args.path, '{}.json'.format(iso_date))
        legal_paths.add(data_file_path)
        if not os.path.isfile(data_file_path):
            # Download the required file
            get_url = GET_URL_TEMPLATE.format(date=iso_date)
            downloaded_data = requests.get(get_url).json()
            with open(data_file_path, 'w') as f_out:
                f_out.write(json.dumps(downloaded_data))

    # Remove illegal files
    for detected_path in (os.path.join(args.path, f)
                          for f in os.listdir(args.path)):
        if detected_path not in legal_paths:
            os.remove(detected_path)


if __name__ == '__main__':
    main()