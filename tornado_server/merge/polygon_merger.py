# === POLYGON MERGE ALGORITHM ===
#
#   ~By Tom Wyllie~
#
#   I don't know if this algorithm already existed; there may be a more
#   efficient of this "polygon merge algorithm" functionality somewhere else,
#   but I've written this as a hopefully useful script for the functionality
#   I require.
#
#
#   Given a list of coordinates, compute the Delaunay triangulation and merge
#   the triangles iteratively by removing edges to create larger polygons,
#   until a desired number of polygons remain.
#

import collections
import math

import numpy as np
from scipy.spatial import Delaunay

# noinspection PyPackageRequirements
import matplotlib.pyplot as plt
# noinspection PyPackageRequirements
from matplotlib.patches import Polygon
# noinspection PyPackageRequirements
from matplotlib.collections import PatchCollection


def merge_polygons(coords, iterations=None, requested_polygons=None,
                   max_d=None, keep_all_iters=False):
    if not (requested_polygons or iterations):
        raise RuntimeError('Must specify number of polygons to generate, or'
                           'maximum number of iterations for algorithm!')

    # Use SciPy to compute Delaunay triangulation
    triangulation = Delaunay(coords).simplices

    if max_d:
        triangulation = [t for t in triangulation if below_d(t, coords, max_d)]

    paths = (tuple(v) for v in triangulation)
    areas = compute_triangle_areas(coords, triangulation)

    if iterations is None and requested_polygons:
        iterations = len(triangulation) - requested_polygons

    polygons = {frozenset(p): get_polygon(p, a) for (p, a) in zip(paths, areas)}

    # View before merging, for debug purposes
    # plot_polygons(coords, polygons)

    all_iters = []

    if keep_all_iters:
        all_iters.append(polygons.copy())

    for iter_count in range(iterations):
        print('=== ITERATION {:d}/{:d}'.format(iter_count + 1, iterations))
        print('Processing {:d} polygons...'.format(len(polygons)))

        # Find the polygon with the smallest area
        smallest_polygon = min(polygons, key=lambda x: polygons[x].area)
        print('Smallest polygon;')
        print('\tPolygon path {}'.format(polygons[smallest_polygon].path))

        # Find the polygon that shares the greatest length of edge with the
        #   smallest polygon
        neighbour = find_neighbour(polygons, coords, smallest_polygon)
        if not neighbour:
            print('No more neighbours found! Only one polygon is left; no more '
                  'iterations can be completed.')
            break
        print('edgiest neighbour ', polygons[neighbour].path)

        # Merge the smallest polygon with its edgiest neighbour
        merge_neighbours(polygons, smallest_polygon, neighbour)

        if keep_all_iters:
            all_iters.append(polygons.copy())

        print('{:d} polygons remaining'.format(len(polygons)))

    # View after merging, for debug purposes
    # plot_polygons(coords, polygons)
    # plt.show()

    if keep_all_iters:
        return all_iters
    return polygons


def merge_neighbours(polygons, poly_1, poly_2):
    """Merge two neighbouring polygons."""

    path_1 = list(polygons[poly_1].path)
    path_2 = list(polygons[poly_2].path)
    print('path 1 ', path_1)
    print('path 2 ', path_2)

    # Align paths for merging
    path_1, path_2, last_shared = align_paths(path_1, path_2)

    # Do the magic, and merge the paths of the two polygons
    new_path = [path_1[0]] + path_1[:last_shared:-1] + path_2[last_shared:]
    print('new path', new_path)

    # Compute the new area
    new_area = polygons[poly_1].area + polygons[poly_2].area

    # Find the new members of the polygon by unifying sets
    new_members = polygons[poly_1].members | polygons[poly_2].members
    new_members |= set(new_path)

    # Remove the old, and now merged, polygons.
    polygons.pop(poly_1)
    polygons.pop(poly_2)

    # Update the polygons dictionary with this new data
    new_polygon = get_polygon(new_path, new_area, members=new_members)
    polygons[frozenset(new_path)] = new_polygon


def align_paths(path_1, path_2):
    """Given two lists of integers, and without changing the order of elements
        relative to each other (but treating the list as a closed loop), and
        allowing the reversal of the order of the path, align the two lists
        such that the greatest possible number of elements are shared between
        indices the two lists, at the start of the list.

        For example, aligning paths [4, 0, 2] and [7, 2, 4, 0] would give
        [2, 4, 0] and [2, 4, 0, 7] or equivalently, [0, 4, 2] and [0, 4, 2, 7]


        """
    # Get the vertices that are common to both polygons
    shared_vertices = set(path_1) & set(path_2)

    # Determine if both lists of vertices (ie paths) are stored traversing the
    #   path in the same direction or not.
    iter_vs = iter(shared_vertices)
    arbitrary_shared_vertex = next(iter_vs)

    i_1 = path_1.index(arbitrary_shared_vertex)
    i_2 = path_2.index(arbitrary_shared_vertex)
    for step in (-1, 1):
        if path_1[(i_1 + step) % len(path_1)] == path_2[
                  (i_2 + step) % len(path_2)]:
            break
    else:
        # No step in either direction matched; the directions must differ.
        #   Reverse one of the paths such that they both traverse the shared
        #   boundary in the same direction.
        path_2 = path_2[::-1]

    # Find the index of the 'first' point that lies on the shared boundary
    on_boundary = True
    # We must choose the longer path here, or there is a possibility all points
    #   in the path we are iterating through will be shared.
    longest_path = path_1 if len(path_1) >= len(path_2) else path_2
    first_shared_value = longest_path[0]
    for v in longest_path:
        if v not in shared_vertices:
            on_boundary = False
        if v in shared_vertices and not on_boundary:
            first_shared_value = v
            break

    # 'Pull' both iterables so that the 'first' point is at the start
    path_1 = pull_list(path_1, path_1.index(first_shared_value))
    path_2 = pull_list(path_2, path_2.index(first_shared_value))

    last_shared = len(shared_vertices) - 1
    if path_1[:last_shared] != path_2[:last_shared]:
        raise Exception('Can\'t merge two polygons with multiple boundaries!')

    print('processed path 1 ', path_1)
    print('processed path 2 ', path_2)
    return path_1, path_2, last_shared


def pull_list(a, i):
    # Bring the desired element to the start of the list, keeping elements in
    #   order around the closed loop.
    return a[i:] + a[:i]


def find_neighbour(polygons, coords, base_polygon):
    # Given a polygon, find all of its neighbours, and return the neighbour with
    #   which it shares the greatest total length along the boundary.

    # Create a list of all polygons the share at least one vertex with the
    #   smallest polygon. We can then search this list for matching edges,
    #   without having to iterate through the large polygons list again.
    common_vertices = [p for p in polygons if p & base_polygon
                       and p is not base_polygon]

    path = polygons[base_polygon].path
    smallest_edges = [{path[i], path[(i + 1) % len(path)]}
                      for i in range(len(path))]

    # A defaultdict is used to we can include multiple edges for a single
    #   neighbour.
    neighbours = collections.defaultdict(int)
    for edge in smallest_edges:
        # There can only be one polygon sharing each edge
        try:
            neighbour = next(p for p in common_vertices if p > edge)
        except StopIteration:
            # There were no neighbours for this edge
            continue
        neighbours[neighbour] += dist_between_vertices(coords, edge.pop(),
                                                       edge.pop())

    # Return the neighbour that shares the longest length of edges in total (if
    #   any neighbours were found)
    if neighbours:
        return max(neighbours, key=neighbours.get)


def get_polygon(path, area, members=None):
    if members is None:
        members = set(path)
    poly = collections.namedtuple('polygon', ('path', 'area', 'points'))
    poly.path = path
    poly.area = area
    poly.members = members
    return poly


def compute_triangle_areas(coords, triangles):
    """Generator to compute triangle areas"""
    for triangle in triangles:
        vertex_coords = [coords[i] for i in triangle]
        yield triangle_area(vertex_coords)


def triangle_area(coords):
    # Compute area of a triangle - reused from Michaelmas notebook :)
    x, y = [[c[i] for c in coords] for i in (0, 1)]
    # Iterate through, and sum, the combinations of x and y on the numerator
    #   of the function for area.
    prod = sum(xn * (y[(i + 2) % 3] - y[(i + 1) % 3]) for i, xn in enumerate(x))
    return abs(prod * 0.5)


def dist_between_vertices(coords, v1, v2):
    # Helper function to find the distance between two points, given by their
    #   indices in the coords list.
    c1, c2 = coords[v1], coords[v2]
    dx = c2[0] - c1[0]
    dy = c2[1] - c1[1]
    return math.sqrt(dx ** 2 + dy ** 2)


def below_d(poly, coords, d):
    return all((dist_between_vertices(coords, poly[i % 3], poly[(i + 1) % 3])
                <= d) for i in range(3))


def plot_polygons(coords, polygons, annotate=False, log_members=False):
    fig, ax = plt.subplots()
    patches = []

    print('Processing {:d} polygons...'.format(len(polygons)))

    for p in polygons.values():
        if log_members:
            print('{:d} Members: '.format(len(p.members)), p.members)
        path = np.asarray([coords[v][::-1] for v in p.path])
        polygon = Polygon(path, fill=True, linestyle='solid')
        patches.append(polygon)

    p = PatchCollection(patches, alpha=0.4)
    colors = np.linspace(start=0, stop=100, num=len(patches))
    np.random.seed(12)
    np.random.shuffle(colors)
    p.set_array(np.array(colors))
    ax.add_collection(p)

    axes = plt.gca()

    y = coords[:,0]
    x = coords[:,1]
    axes.set_xlim([x.min(), x.max()])
    axes.set_ylim([y.min(), y.max()])

    if annotate:
        for i, coord in enumerate(coords):
            plt.annotate(i, xy=coord)


# Comment out to unsuppress print statements
# noinspection PyShadowingBuiltins
def print(*args, **kwargs):
    pass


if __name__ == '__main__':
    MAX = 1000
    MIN = -MAX

    n = 1000
    rand_x = np.random.randint(low=MIN, high=MAX, size=n)
    rand_y = np.random.randint(low=MIN, high=MAX, size=n)
    demo_coords = [c for c in zip(rand_x, rand_y)]

    demo_request = 20
    merge_polygons(demo_coords, requested_polygons=demo_request)
