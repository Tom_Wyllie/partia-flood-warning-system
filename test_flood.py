from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold, \
    stations_highest_rel_level


def dummy_station_list_creator():
    test_station1 = MonitoringStation("001", "m-id-1", "station-1",
                                      (1, 1), (1, 3), "Mississippi", "Jackson")

    test_station2 = MonitoringStation("002", "m-id-2", "station-2",
                                      (1, 2), (0.5, 1.5), "Nile", "Cairo")

    test_station3 = MonitoringStation("003", "m-id-3", "station-3",
                                      (2, 34), (4, 3), "Nile", "Khartoum")

    test_station4 = MonitoringStation("004", "m-id-4", "station-4",
                                      (2, 34), (3, 12), "Thames", "London")

    test_station5 = MonitoringStation("005", "m-id-5", "station-5",
                                      (2, 34), (10, 25), "Thames", "Reading")

    test_station1.latest_level = 1.5
    test_station2.latest_level = 1
    test_station3.latest_level = 7.5
    test_station4.latest_level = 6
    test_station5.latest_level = 17

    return [test_station1, test_station2, test_station3, test_station4,
            test_station5]


def test_stations_level_over_threshold():
    test_stations = dummy_station_list_creator()

    # generating the output using indicative threshold of 0.4
    generated_output = stations_level_over_threshold(test_stations, 0.4)

    # expected output based on manual calculation
    expected_output = [(test_stations[1], 0.5), (test_stations[4], 7 / 15)]

    assert (generated_output == expected_output)


def test_stations_highest_rel_level():
    test_stations = dummy_station_list_creator()

    # generating the output using N = 3
    generated_output = stations_highest_rel_level(test_stations, 2)

    # expected output based on manual calculation
    expected_output = [test_stations[1], test_stations[4]]

    assert (generated_output == expected_output)
