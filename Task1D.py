from floodsystem.stationdata import build_station_list
from floodsystem import geo


def run():
    """Requirements for Task 1A"""

    # Build list of all stations
    stations = build_station_list()

    # list of rivers with at least one station:
    rivers_with_station = geo.rivers_with_station(stations)
    first_stations = sorted(rivers_with_station)[:10]
    print("Number of rivers with at least 1 station: {}"
          .format(len(rivers_with_station)))
    print("First 10 rivers with at least one station: {}"
          .format(first_stations))

    river_to_station_dict = geo.stations_by_river(stations)
    aire_stations = sorted([station.name
                            for station in
                            river_to_station_dict.get("River Aire")])
    cam_stations = sorted([station.name
                           for station in
                           river_to_station_dict.get("River Cam")])
    thames_stations = sorted([station.name
                              for station in
                              river_to_station_dict.get("Thames")])

    print("Stations on River Aire: {}".format(aire_stations))
    print("Stations on River Cam: {}".format(cam_stations))
    print("Stations on Thames: {}".format(thames_stations))


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
