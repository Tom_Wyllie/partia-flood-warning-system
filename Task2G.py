from floodsystem.warning import station_to_warning
from floodsystem.stationdata import build_station_list, update_water_levels


def run():
    stations = build_station_list()
    update_water_levels(stations)

    for station in stations:
        if station_to_warning(station) == "severe":
            print("Station {} is at a severe level of flooding".format(station.name))


if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()
