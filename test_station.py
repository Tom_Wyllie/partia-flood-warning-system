from floodsystem import station


def test_monitoring_station():

    # Create a station
    s_id = 'test-s-id'
    m_id = 'test-m-id'
    label = 'some station'
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = 'River X'
    town = 'My Town'

    station_attrs = [s_id, m_id, label, coord, trange, river, town]
    s = station.MonitoringStation(*station_attrs)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    assert s.typical_range_consistent()

    # Make a new station object, this time with some inconsistent typical ranges
    inconsistent_tranges = [(7.3, -12), {}, '', None, 4, (3.2,)]
    for bad_trange in inconsistent_tranges:
        station_attrs[4] = bad_trange
        s = station.MonitoringStation(*station_attrs)
        assert not s.typical_range_consistent()
