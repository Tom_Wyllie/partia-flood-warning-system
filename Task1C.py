from floodsystem.stationdata import build_station_list
from floodsystem import geo


def run():
    cambridge_coordinates = (52.2053, 0.1218)  # Cambridge coordinates
    stations = build_station_list()  # Building list of all stations

    # Find stations within a distance of 10km of Cambridge:
    stations_near_cambridge = geo.stations_within_radius(stations,
                                                         cambridge_coordinates,
                                                         10)

    # Sorts Cambridge stations by name
    stations_names = sorted(
        [station.name for station in stations_near_cambridge])

    print(stations_names)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()
