from floodsystem.stationdata import build_station_list
from floodsystem import geo


def run():
    """Requirements for Task 1A"""

    # Build list of all stations
    stations = build_station_list()

    # list of the 9 most covered rivers, by station count:
    most_covered_rivers = geo.rivers_by_station_number(stations, 9)

    print("The nine most covered rivers are: {}".format(most_covered_rivers))


if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()
