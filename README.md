# CUED Part IA Flood Warning System
---
This is the Part IA Lent Term computing activity as developed by Tom Wyllie and Hannan Saddiq (lab group 120).

Most of our work this term has focused on extension activities, and our flood monitoring system can be viewed at http://flood-monitoring.tk:8002/.

**The polygon merge algorithm is located in tornado/merge/polygon_merger.py**

**The HDBSCAN clustering algorithm is located in floodsystem/clustering.py**

* /ClusteringDemo.py generates a plot.ly web map that has been embedded in the above website

**The key files that keep the website running are as follows;**

* tornado_server/server.py: contains all the HTTP handlers for the server, and the Tornado server itself.
* tornado\_server/update\_data.py: maintains an up-to-date copy of the last n days of flood level data from the online API.
* tornado_server/data\_daemon.py:  of raw python, and a multi-million element numpy matrix used to process the half gigabyte or so of each day morning.

**Various client-side files have also been developed;**

 * tornado_server/js/gmap.js: the client side animation and rendering on the Google Map.
 * tornado_server/index.html: the webpage itself.
