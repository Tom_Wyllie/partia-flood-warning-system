from floodsystem import stationdata
from floodsystem import station


def run():
    """Requirements for Task 1F"""

    # Build list of stations
    stations = stationdata.build_station_list()

    # Get list of stations with inconsistent data
    inc_stats = station.inconsistent_typical_range_stations(stations)

    # Extract names
    inc_stat_names = sorted(stn.name for stn in inc_stats)

    # Get all stations with inconsistent data, format and print them out
    description = '\nAll stations with currently inconsistent range data;'
    print('\n  '.join((description, *inc_stat_names)))


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()
