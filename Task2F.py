import datetime

from floodsystem import analysis
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels

import matplotlib.pyplot as plt


def run():
    # Build list of stations
    stations = build_station_list()

    # Update water levels
    update_water_levels(stations)

    top_five_stations = stations_highest_rel_level(stations, 5)

    dt = datetime.timedelta(days=2)
    for station in top_five_stations:
        dates, levels = fetch_measure_levels(station.measure_id, dt=dt)

        poly = analysis.polyfit(dates, levels, 4)
        plt.plot(dates, levels)

        # Polyfitted data
        polyfitted_levels = [poly(i) for i, d in enumerate(dates)]
        plt.plot(dates, polyfitted_levels)

        plt.axhline(y=station.typical_range[0])
        plt.axhline(y=station.typical_range[1])
        plt.show()


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
